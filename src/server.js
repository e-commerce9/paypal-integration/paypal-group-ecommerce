const express = require('express');
const {static} = require('express');
const bodyParser = require('body-parser');
const server = express();
const {join} = require('path');
const appController = require('./controller/app.controller');

server.set('view engine', 'hbs');
server.set('views', join(__dirname,'..', 'views'));
server.use(static(join(__dirname, '..', 'public')));

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended: true}));

server.use(appController);
server.listen(process.env.PORT | 3005);
console.log(`Server started with port 3005 ...`);
